Movie App
==============
Use keyboard arrows or swipe to navigate.

## Demo

https://dazzling-keller-1f83ea.netlify.com

https://vimeo.com/287970972


## Installation Instructions

Clone repository

```
git clone https://ninaphilippova@bitbucket.org/ninaphilippova/movie-app.git
```
Run
```
npm init
```
Start webpack dev server
```
npm start
```
or build
```
npm run build
```
Test
```
npm test
```

## Features

✔️ Load from API and display a list of videos in a scrollable horizontal carousel on the home page. Each tile must display a movie title and an associated cover image (look it up in the response).

✔️ User should be able to select a video and play it back in full screen. When playback is finished or user quits it, user must be taken back to home page.

✔️ Display second “Previously watched” carousel on the home page. It must be updated and re-sorted according to the most recently watched video.

✔️ The user should be able to use a mouse and keyboard (arrows/Enter keys) to select the video.

✔️ Layout size adjustment. The application must be able to adjust layout proportionally based on the desktop browser width.

✔️ Responsive design. Change carousel to Portrait view grid if application is run on mobile device.

✔️ Content list refresh button. Each click reloads content from API

❌ Image caching. Upon application restart or content refresh (if implemented) previously downloaded image is loaded from the local device cache.

❌ Error handling. Note that if you simulate any errors, such as invalid movie item, please document precisely in the accompanied Readme file.

✔️ Persistent storage of watched items. You can use your custom server, database or local device cache. Note that you don’t need to store actual movie files.

✔️ Unit tests. It is up to you which testing framework to use.


## Known Issues

🐞 When the player is displayed, users can still use arrows/enter to select/play movies. Arrows should probably be used for starting and controlling the player.

🐞 When a movie is added to the second carousel, it is no longer focused. Focus should stay on the original movie.

🐞 No "no movies" message is displayed when there are no movies to display.

🐞 No bounce when swiping to the end of the carousel


# License

MIT Licensed