async function fetchSafe(...args) {
    const result = await fetch(...args);
    if (!result.ok) {
        throw new Error(`Request failed: ${result.status} ${result.statusText}.`);
    }
    return result;
}
export default fetchSafe;