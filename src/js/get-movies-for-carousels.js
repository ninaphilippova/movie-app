import { getStartedWatchingIds } from './storage.js';

function getMoviesForCarousels(allMovies) {
    const startedWatchingIds = getStartedWatchingIds();
    const newMovies = allMovies.filter(m => !startedWatchingIds.includes(m.id));
    const startedWatching = startedWatchingIds
        .map(id => allMovies.find(m => m.id === id))
        .filter(m => m);
    return {newMovies, startedWatching};
}

export default getMoviesForCarousels;