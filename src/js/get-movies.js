import fetchSafe from './fetch-safe.js';

async function getMovies() {
    const response = await fetchSafe('https://demo2697834.mockable.io/movies');
    const json = await response.json();
    return json.entries.map(item => ({
        id: item.id,
        title: item.title,
        img: item.images[0].url.replace(/^http:\/\//, 'https://'),
        url: item.contents[0].url
    }));
}

export default getMovies;