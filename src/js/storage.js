const localStorage = window.localStorage;

export function saveStartedWatchingIds(array) {
    localStorage.setItem('startedWatchingIds', JSON.stringify(array));
}

export function getStartedWatchingIds() {
    const stored = localStorage.getItem('startedWatchingIds');
    return stored ? JSON.parse(stored) : [];
}