import React from 'react';
import lists from '../js/lists.js';
import getMovies from '../js/get-movies.js';
import getMoviesForCarousels from '../js/get-movies-for-carousels.js';
import { saveStartedWatchingIds } from '../js/storage.js';
import MovieCarousel from './MovieCarousel.jsx';
import Player from './Player.jsx';

class App extends React.Component {
    constructor() {
        super();

        this.addToStartedWatching = this.addToStartedWatching.bind(this);
        this.showHidePlayer = this.showHidePlayer.bind(this);
        this.fetchMovies = this.fetchMovies.bind(this);
        this.handleKey = this.handleKey.bind(this);
        this.setFocusedCarouselIndex = this.setFocusedCarouselIndex.bind(this);

        this.movies = {
            allMovies: [],
            newMovies: [],
            startedWatching: []
        };
        this.state = {
            error: false,
            loading: true,
            playing: false,
            carousels: [],
            focusedCarouselIndex: 0
        };
    }

    componentDidMount() {
        this.fetchMovies();
    }

    async fetchMovies() {
        try {
            const allMovies = await getMovies();
            const {newMovies, startedWatching} = getMoviesForCarousels(allMovies);
            this.movies = { newMovies, startedWatching, allMovies };
            this.setState({ loading: false });
            this.updateCarouselsState();
        }
        catch (e) {
            this.setState({ error: e.toString(), loading: false });
        }
    }

    addToStartedWatching(movie) {
        const startedWatching = this.movies.startedWatching;
        const newMovies = this.movies.newMovies;
        for (const item of [startedWatching, newMovies]) {
            const index = item.findIndex(m => m.id === movie.id);
            if (index >= 0) {
                item.splice(index, 1);
            }
        }
        startedWatching.unshift(movie);
        saveStartedWatchingIds(startedWatching.map(m => m.id));

        //this.setState({ focusedCarouselIndex: 0 });
        this.updateCarouselsState();
    }

    updateCarouselsState() {
        const carousels = lists
            .map(({ id, title }) => ({ id, title, movies: this.movies[id] }))
            .filter(c => c.movies.length > 0);
        this.setState({ carousels });
    }

    showHidePlayer(movie) {
        this.setState({ playing: movie });
    }

    handleKey(e) {
        // TODO: reuse common code in handler in movie carousel
        const currentIndex = this.state.focusedCarouselIndex;
        if (e.which === 38 /* up */) {
            const focusedCarouselIndex  = (currentIndex === 0) ? currentIndex :  currentIndex - 1;
            this.setState({ focusedCarouselIndex });
        }

        if (e.which === 40 /* down */) {
            const focusedCarouselIndex = (currentIndex === this.state.carousels.length - 1) ? currentIndex : currentIndex + 1;
            this.setState({ focusedCarouselIndex });
        }
    }

    setFocusedCarouselIndex(index) {
        this.setState({ focusedCarouselIndex: index });
    }

    renderContent() {
        const { error, loading, playing } = this.state;
        const player = playing ? <Player movie={playing} addToStartedWatching={this.addToStartedWatching} showHidePlayer={this.showHidePlayer}/> : '';

        if (error) {
            return <div>{error}</div>;
        }

        if (loading) {
            return <div>Loading...</div>;
        }

        const carousels = this.state.carousels.map((c, index) => {
            return <MovieCarousel
                key={c.id}
                movies={c.movies}
                carouselTitle={c.title}
                showHidePlayer={this.showHidePlayer}
                focused={index === this.state.focusedCarouselIndex}
                onFocus={() => this.setFocusedCarouselIndex(index)}/>;
        });

        return <div onKeyUp={this.handleKey}>
            {carousels}
            {player}
        </div>;
    }

    render() {
        return (
            <div className='app'>
                <header>
                    <button onClick={this.fetchMovies} className='refresh-button'>Refresh</button>
                </header>
                <main>
                    {this.renderContent()}
                </main>
            </div>
        );
    }
}

export default App;