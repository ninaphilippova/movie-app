import React from 'react';
import MovieImage from '../assets/movie.jpg';
import ReactImageFallback from 'react-image-fallback';

class Movie extends React.Component {
    constructor(props) {
        super(props);
        this.elementRef = React.createRef();
        this.imgRef = React.createRef();
        this.showHidePlayer = this.showHidePlayer.bind(this);
        this.handleKey = this.handleKey.bind(this);
    }

    componentDidMount() {
        this.focus();
    }

    componentDidUpdate() {
        this.focus();
    }

    focus() {
        if (!this.props.focused) {
            return;
        }

        this.elementRef.current.focus();
        setTimeout(() => {
            this.elementRef.current.parentElement.scrollIntoView(false);
        }, 0);
    }

    // Drag is conflicting with swipe, should be prevented
    cancelDrag(e) {
        e.preventDefault();
        //console.log('drag detected!');
    }

    showHidePlayer() {
        this.props.showHidePlayer(this.props.movie);
    }

    handleKey(e) {
        if (e.which !== 13 /* enter */) {
            return;
        }
        this.showHidePlayer();
    }

    render() {
        const img = this.props.movie.img;

        return (
                <div
                    className=' movie-card'
                    ref={this.elementRef}
                    tabIndex='0'
                    onKeyPress={this.handleKey}
                    onFocus={this.props.onFocus}>
                        <ReactImageFallback
                            src={img}
                            fallbackImage={MovieImage}
                            initialImage={MovieImage}
                            onClick={this.showHidePlayer}
                            onDragStart={this.cancelDrag}/>
                        <div className='movie-title' title={this.props.movie.title}>
                            {this.props.movie.title}
                        </div>
                </div>
        );
    }
}

export default Movie;