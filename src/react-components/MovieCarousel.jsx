import React from 'react';
import Movie from './Movie.jsx';
import Hammer from 'hammerjs';

class MovieCarousel extends React.Component {
    constructor(props) {
        super(props);
        this.handleSwipe = this.handleSwipe.bind(this);
        this.handleKey = this.handleKey.bind(this);
        this.elementRef = React.createRef();
        this.setFocusedMovieIndex = this.setFocusedMovieIndex.bind(this);
        this.state = {
            //movies: this.props.movies,
            focusedMovieIndex: 0,
            focused: this.props.focused
        };
    }

    componentDidMount() {
        this.hammer = new Hammer(this.elementRef.current);
        this.hammer.on('swipe', this.handleSwipe);
        //document.addEventListener('keydown', e => this.handleKey(e.key, e));
    }

    componentWillUnmount() {
        this.hammer.off('swipe');
    }

    handleSwipe(e) {
        if (e.direction !== Hammer.DIRECTION_LEFT && e.direction !== Hammer.DIRECTION_RIGHT) {
            return;
        }

        // it looks like 1.8 gives just enough kick for swipe
        this.elementRef.current.scrollLeft -= 1.8 * e.deltaX;
    }

    handleKey(e) {
        const currentIndex = this.state.focusedMovieIndex;

        if (e.which === 37 /* left */) {
            const focusedMovieIndex = (currentIndex === 0) ? currentIndex :  currentIndex - 1;
            this.setState({ focusedMovieIndex });
        }

        if (e.which === 39 /* right */) {
            const focusedMovieIndex = (currentIndex === this.props.movies.length - 1) ? currentIndex : currentIndex + 1;
            this.setState({ focusedMovieIndex });
        }
    }

    setFocusedMovieIndex(index) {
        this.props.onFocus();
        this.setState({ focusedMovieIndex: index });
    }
    /* <Movie
                    movie={m}
                    index={index}
                    focused={this.props.focused && index === this.state.focusedMovieIndex}
                    focusScrollMode='parent'
                    showHidePlayer={this.props.showHidePlayer}
                    setFocusedMovieIndex={this.setFocusedMovieIndex}/> */

    render() {
        const carouselTitle = this.props.carouselTitle;
        const carousel = this.props.movies.map((m, index) =>
            <div data-id={m.id} key={m.id} className='carousel-item'>
                <Movie
                    movie={m}
                    focused={this.props.focused && index === this.state.focusedMovieIndex}
                    showHidePlayer={this.props.showHidePlayer}
                    onFocus={() => this.setFocusedMovieIndex(index)}/>
            </div>
        );

        return (
            <div>
                <div className='carousel-title'>
                    {carouselTitle}
                </div>
                <div className='carousel' ref={this.elementRef} onKeyDown={this.handleKey}>
                    {carousel}
                </div>
            </div>
        );
    }
}

export default MovieCarousel;