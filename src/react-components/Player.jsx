import React from 'react';

class Player extends React.Component {
    constructor(props) {
        super(props);
        this.handleClose = this.handleClose.bind(this);
        this.handleOnPlay = this.handleOnPlay.bind(this);
    }

    handleClose() {
        this.props.showHidePlayer(false);
    }

    handleOnPlay() {
        const movie = this.props.movie;
        this.props.addToStartedWatching(movie);
    }

    render() {
        return (
            <div className='player-backdrop'>
                <div className='player-pane'>
                    <div className='player-pane-controls'>
                        <button onClick={this.handleClose} className='close-button'>Close</button>
                    </div>
                    <video controls
                        muted
                        onPlay={this.handleOnPlay}
                        onEnded={this.handleClose}
                        src={this.props.movie.url}>
                        Sorry, your browser does not support embedded videos.
                    </video>
                </div>
            </div>
        );
    }
}

export default Player;