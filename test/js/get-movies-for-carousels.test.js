jest.mock('../../src/js/storage.js');

import getMovies from '../../src/js/get-movies-for-carousels.js';

const movies = [{ id: '5',  title: '77' }, { id: 5,  title: '77' } ];


test('started watching videos are removed from new movies list', () => {
    const { getStartedWatchingIds } = require('../../src/js/storage.js');
    getStartedWatchingIds.mockImplementation(() => [movies[0].id]);

    const { newMovies} = getMovies(movies);

    expect(newMovies).toEqual([movies[1]]);
});

test('no new movies if all watched', () => {
    const { getStartedWatchingIds } = require('../../src/js/storage.js');
    getStartedWatchingIds.mockImplementation(() => [movies[0].id, movies[1].id]);

    const { newMovies, startedWatching } = getMovies(movies);

    expect(newMovies.length).toEqual(0);
    expect(startedWatching).toEqual(movies);
});

test('correctly identifies watched movies', () => {
    const { getStartedWatchingIds } = require('../../src/js/storage.js');
    getStartedWatchingIds.mockImplementation(() => [movies[0].id] );

    const { startedWatching } = getMovies(movies);

    expect(startedWatching).toEqual([movies[0]]);
});

test('watched movies that are no longer available are removed', () => {
    const { getStartedWatchingIds } = require('../../src/js/storage.js');
    getStartedWatchingIds.mockImplementation(() => ['55'] );

    const { newMovies, startedWatching } = getMovies(movies);

    expect(newMovies).toEqual(movies);
    expect(startedWatching.length).toEqual(0);
});

test('sort order for movies user has started watching is preserved', () => {
    const { getStartedWatchingIds } = require('../../src/js/storage.js');
    getStartedWatchingIds.mockImplementation(() => [ 5, '55', '5'] );

    const { startedWatching } = getMovies(movies);

    expect(startedWatching.length).toEqual(2);
    expect(startedWatching[0].id).toEqual(5);
    expect(startedWatching[1].id).toEqual('5');
});