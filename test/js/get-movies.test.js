jest.mock('../../src/js/fetch-safe.js');

import getMovies from '../../src/js/get-movies.js';

test('mapping is correct', async () => {
    const movieData = {entries:[{
        title: 'some title',
        contents: [{ url: '1' }],
        images: [{ url: '2' }],
        id: 1
    }]};

    const fetchSafe = require('../../src/js/fetch-safe.js').default;
    fetchSafe.mockImplementation(() => Promise.resolve({
        json: () => Promise.resolve(movieData)
    }));

    const movies = await getMovies();
    expect(movies).toEqual([{id: 1, title: 'some title', img: '2', url: '1'}]);
});