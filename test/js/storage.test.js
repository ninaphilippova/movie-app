import {saveStartedWatchingIds, getStartedWatchingIds}  from '../../src/js/storage.js';

const ids = ['5', '77', 5 ];

test('empty array is returned if nothing is in local storage yet', () => {
    localStorage.clear();
    const startedWatching = getStartedWatchingIds();
    expect(startedWatching.length).toEqual(0);
});

test('data is saved and returned from local storage', () => {
    saveStartedWatchingIds(ids);
    const startedWatching = getStartedWatchingIds();
    expect(startedWatching).toEqual(expect.arrayContaining(ids));
});


