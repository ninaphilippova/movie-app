jest.mock('../../src/js/get-movies.js');
jest.mock('../../src/js/get-movies-for-carousels.js');
jest.mock('../../src/js/lists.js', () => ([
    { id: 'allMovies', title: 'All movies'},
    { id: 'newMovies', title: 'Movies for you' },
    { id: 'startedWatching', title: 'Started watching' }
]));

import React from 'react';
import Enzyme from 'enzyme';
import App from '../../src/react-components/App';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

function waitForAllPromisesToResolve() {
    return new Promise(resolve => process.nextTick(() => resolve()));
}

test('should show movies', async () => {
    const movies = [
        {id: 1, title: 'some title', img: '2', url: '1'}
    ];
    const getMovies = require('../../src/js/get-movies').default;
    getMovies.mockImplementation(() => Promise.resolve(movies));

    const getMoviesForCarousels = require('../../src/js/get-movies-for-carousels.js').default;
    getMoviesForCarousels.mockImplementation(() => ({
        newMovies: [],
        startedWatching: []
    }));

    const app = Enzyme.mount(<App/>);
    await waitForAllPromisesToResolve();

    expect(app.html()).toMatch(/some title/);
});

test('no carousels if no movies', async () => {
    const getMovies = require('../../src/js/get-movies').default;
    getMovies.mockImplementation(() => Promise.resolve([]));

    const getMoviesForCarousels = require('../../src/js/get-movies-for-carousels.js').default;
    getMoviesForCarousels.mockImplementation(() => ({
        newMovies: [],
        startedWatching: []
    }));

    const app = Enzyme.mount(<App/>);
    await waitForAllPromisesToResolve();

    expect(app.html()).not.toMatch(/carousel-title/);
});

test('should show error', async () => {
    const getMovies = require('../../src/js/get-movies').default;
    getMovies.mockImplementation(() => Promise.reject(new Error('test error')));

    const app = Enzyme.mount(<App/>);
    await waitForAllPromisesToResolve();
    expect(app.html()).toMatch('test error');
});

test('should show loading', async () => {
    const app = Enzyme.mount(<App/>);
    expect(app.html()).toMatch(/Loading\.\.\./);
});