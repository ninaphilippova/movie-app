/* eslint-env node */
const HtmlWebPackPlugin = require('html-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html'
});

module.exports = {
    entry: './src/react-components/index.jsx',
    output: {
        path: `${__dirname}/output`
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                ]
            },{
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: { loader: 'babel-loader' }
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                  {
                    // workaround for https://stackoverflow.com/questions/44067537/enzyme-render-fails-when-importing-image-with-webpack-while-testing-with-jest
                    loader: 'file-loader',
                    options: {}
                  }
                ]
              }
        ]
    },
    plugins: [htmlPlugin]
};